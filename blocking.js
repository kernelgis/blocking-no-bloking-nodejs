const { getMotoSync } = require('./moto/motos');

console.time('tiempoEjecucion');
console.log('Iniciando');

const moto1 = getMotoSync(1);
console.log('Moto 1', moto1);

const moto2 = getMotoSync(2);
console.log('Moto 2', moto2);

console.log('Finalización');
console.timeEnd('tiempoEjecucion');
