const motos = [
  {
    id: 1,
    marca: 'Indian',
    cc: 500
  },
  {
    id: 2,
    marca: 'Harley',
    cc: 400
  },
  {
    id: 3,
    marca: 'Ducati',
    cc: 600
  }
];

function findMoto(id) {
  return motos.find(x => x.id === id);
}

function getMotoSync(id) {
  const puntoInicio = new Date().getTime();
  let moto = null;
  while(new Date().getTime() - puntoInicio <= 4000) {
    // Realiza consultas a la base de datos 
    moto = findMoto(id);
  }
  return moto;
}

function getMoto(id, cb) {
  
  setTimeout(()=> {
    const moto = findMoto(id); // encuentra el registro con el id
    cb(moto)
  }, 4000) // retorna despues de 4 seg.
}

module.exports = {
 getMoto, getMotoSync 
}