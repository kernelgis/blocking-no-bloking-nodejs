const { getMoto } = require('./moto/motos');

console.time('tiempoEjecucion');
console.log('Iniciando');

getMoto(1, moto => console.log(moto));
// console.log('Moto 1', moto1);

getMoto(2, moto => console.log(moto));
// console.log('Moto 2', moto2);

console.log('Finalización');
console.timeEnd('tiempoEjecucion');


